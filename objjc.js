const 
acornObj = require('acorn-objj'),
compiler = require("./lib/compiler.js"),
codeGenerator = require("./lib/code-generator.js"), 
issueHandler = require("acorn-issue-handler");
 

exports.compile = function(source, sourcePath) {

	//acorn options
	var options = {		
		sourceType : 'module'
	};

	var  issues = new issueHandler.IssueList(),
		 ast = null;
	
	
	 try {
	 	 
		 ast = acornObj.parse.parse(source, options, issues);
	
	 } catch (ex) {
         if (issueHandler.isIssue(ex)) {
			 printIssue(ex, sourcePath); 
         }  
         else
             throw ex;
	 }
	 
	
	 if ( ast ) {
		 
	 	//console.log(util.inspect(ast, false, null))

		//compiler options
	 	var cOptions = {
	 		objjScope : false,
            classDefs : new Map(),
            protocolDefs : new Map(),
            typedefs: new Map(),
            importedFiles: new Set()
	 	};

	 	 global.DEBUG = false;

         Object.assign(compiler.defaultOptions, cOptions);
	
	     this.compiler = new compiler.Compiler(source, sourcePath, ast, issues, compiler.defaultOptions);
	     this.compiler.compileWithFormat(codeGenerator);
	 	 
		 if (this.compiler.errorCount > 0) {
		 	 
			 let count = this.compiler.errorCount,
			 	 i = 0,
			  	 errors = false; 
			 
			 
			 for (; i < count; i++) {
				 let ex = this.compiler.issuesList.issues[i];
				 if (ex.severity === 'error') {
				 	 printIssue(ex, sourcePath);
				 	 errors = true;
				 }
			 }	 
			 
			 if (errors)
			 	return;
		 }


	 	return this.compiler.code;
	 }
	
	 return;
};

function printIssue(ex, sourcePath) {

    let buf = [];

    if (ex) {
        buf.push(ex.name + ':');
        if (ex.message) {
            buf.push(ex.message);
        }

        if (ex.source) {
            buf.push('"' + ex.source.trim() + '"\t(' + sourcePath + ', line: ' + ex.lineInfo.line + ', column: ' + ex.lineInfo.column + ')');
        }
    }

    return buf.join("\n");

}




