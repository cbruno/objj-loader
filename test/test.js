const 	fs = require('fs'),
	 	path = require('path'),
		ObjCompiler = require('../objjc.js')


 

runTest('Main.j', 'main.j')

//runTest('Testing class properties', 'objj-nodes/property.j')	
	
//runTest('Array literal inline', 'objj-nodes/array-literal-inline.j');

//runTest('Class declaration', 'objj-nodes/class-declaration.j')

//runTest('Protocol declaration', 'objj-nodes/protocol-declaration.j');

//runTest('Reference-dereference', 'objj-nodes/reference-dereference.j')

//runTest('Property inline', 'objj-nodes/property-inline.j')

function runTest(testName, sourceFile) {
	
	console.log("Running test " + testName);
	
	var sourcePath = path.resolve(__dirname, sourceFile),
		source = fs.readFileSync(sourcePath, 'UTF-8');
		
	
	var result = ObjCompiler.compile(source, sourcePath);

	console.log("<!--- output: --->"); 
	console.log(result); 
	console.log("<!--- end output --->");	
	
}

