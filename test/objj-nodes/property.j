@implementation Properties
{
    int accessors @property;
    int copyMe @property(copy);
    int readOnly @property(readonly);
    int propertyAccessors @property(property=property);
    int getter @property(getter=getMe);
    int setter @property(setter=setMe);
    int getterSetter @property(getter=getMe, setter=setIt);
    int hasGetter @property;
    int hasSetter @property;
    int _underscore @property; // _ is stripped from property names
}

- (int)hasGetter
{
    return self.hasGetter;
}

- (void)setHasSetter:(int)newValue
{
    self.hasSetter = newValue;
}

@end
