const ObjjCompiler = require('./objjc.js'),
      glob = require('glob'),
      fs = require('fs');

module.exports = function OBJCC(source) {

    this.cacheable(true);

	//add in the Objective-J runtime
	var rtPath = require.resolve("./Runtime.js");
	    var header = [
	        // Import the runtime
	        "var objj = require('!!"+rtPath+"');\n"
	    ].join("\n");


	 //add in CPApplicationInfo
     if( !this._compilation.info ) {
        let packageJson = JSON.parse(fs.readFileSync(process.cwd() + "/package.json", 'utf-8'));
        this._compilation.info =  {
            'mainCibFile' : packageJson.mainCibFile
        };

        let info = [
            '(function(global){global.CPApplicationInfo = ',
            JSON.stringify(this._compilation.info),
            ';})(objj_global());'
        ].join("");

        header += info;
     }

     //add in Bundles;
     if(!this._compilation.bundles)   {
        this._compilation.bundles = glob.sync(process.cwd() + "/**/*.cib") ;
     }

     let bundleImports = [];

     this._compilation.bundles.forEach(function(bundlePath){
          bundleImports.push("import '"+bundlePath+"';");
     });

     header += bundleImports.join("\n");

	try {

		let result = ObjjCompiler.compile(source, this.resourcePath);
		return header+result;
	}
	catch (err) {
		console.error(err);
	} 
};